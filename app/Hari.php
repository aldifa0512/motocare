<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hari extends Model
{
    protected $table = 'hari_layanan';

    public function bengkel()
    {
        return $this->belongsTo(Bengekl::class);
    }
}
