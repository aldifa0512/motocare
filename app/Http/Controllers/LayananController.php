<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use JWTAuth;

use App\Bengkel;
use App\Layanan;
use App\User;

class LayananController extends Controller
{
    protected $logged_user;

    public function __construct(){
        // $this->logged_user = JWTAuth::toUser(Input::get('token'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_layanan' => 'required',
            'bobot_layanan' => 'required',
            'harga_layanan' => 'required',
            'bengkel_id' => 'required',
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }

        $nama_layanan = $request->input('nama_layanan');
        $bobot_layanan = $request->input('bobot_layanan');
        $harga_layanan = $request->input('harga_layanan');
        $bengkel_id = $request->input('bengkel_id');

        $bengkel = Bengkel::with('layanan')->where('id', $bengkel_id)->first();
        if(!$bengkel){
            return response()->json([
                'msg' => 'Bengkel not found.'
            ], 401);
        }
            
        $layanan = new Layanan;
        $layanan->nama_layanan = $nama_layanan;
        $layanan->bobot_layanan = $bobot_layanan;
        $layanan->harga_layanan = $harga_layanan;
        $layanan->bengkel_id = $bengkel_id;

        if($layanan->save()){
            $message = [
                'msg' => 'Success created new service.',
                'layanan' => $layanan,
                'delete_layanan' => [
                    'href' => 'api/v1/bengkel/layanan/' . $layanan->id,
                    'method' => 'DELETE'
                ]
            ];
            return response()->json($message, 201);
        }

        $response = [
            'msg' => 'Error during creation'
        ];

        return response()->json($response, 201);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $layanan = Layanan::find($id);
        if(!$layanan){
            return response()->json([
                'msg' => 'Service not found.'
            ], 401);
        }

        if($layanan->delete()){
            $response = [
                'msg' => 'User deleted the service.',
                'add_layanan' => [
                    'href' => 'api/v1/bengkel/layanan',
                    'method' => 'POST',
                    'params' => 'nama_layanan, nama_layanan, bengkel_id, user_id' 
                ]
            ];

            return response()->json($response, 200);
        }

        $response = [
            'msg' => 'Error during deletion'
        ];

        return response()->json($response, 201);
    }
}
