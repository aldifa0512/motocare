<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use JWTAuth;

use App\Bengkel;
use App\Hari;
use App\User;

class HariController extends Controller
{
	protected $logged_user;

    public function __construct(){
        // $this->logged_user = JWTAuth::toUser(Input::get('token'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hari_layanan' => 'required',
            'bengkel_id' => 'required',
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }

        $hari_layanan = $request->input('hari_layanan');
        $bengkel_id = $request->input('bengkel_id');

        $bengkel = Bengkel::with('hari')->where('id', $bengkel_id)->first();
        if(!$bengkel){
            return response()->json([
                'msg' => 'Bengkel not found.'
            ], 401);
        }
            
        $hari = new Hari;
        $hari->hari = $hari_layanan;
        $hari->bengkel_id = $bengkel_id;

        if($hari->save()){
            $message = [
                'msg' => 'Success added new service day.',
                'hari' => $hari,
                'delete_hari' => [
                    'href' => 'api/v1/bengkel/hari/' . $hari->id,
                    'method' => 'DELETE'
                ]
            ];
            return response()->json($message, 201);
        }

        $response = [
            'msg' => 'Error during creation'
        ];

        return response()->json($response, 201);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hari = Hari::find($id);
        if(!$hari){
            return response()->json([
                'msg' => 'Service day not found.'
            ], 401);
        }

        if($hari->delete()){
            $response = [
                'msg' => 'User deleted the service day.',
                'add_hari' => [
                    'href' => 'api/v1/bengkel/hari',
                    'method' => 'POST',
                    'params' => 'hari_layanan, bengkel_id' 
                ]
            ];

            return response()->json($response, 200);
        }

        $response = [
            'msg' => 'Error during deletion'
        ];

        return response()->json($response, 201);
    }
}
