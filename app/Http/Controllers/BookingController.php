<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use JWTAuth;

use App\User;
use App\Bengkel;
use App\Booking;

class BookingController extends Controller
{
    protected $logged_user;

    public function __construct(){
        // $this->logged_user = JWTAuth::toUser(Input::get('token'));
    }

    public function checkBobotLayanan($bobot, $selectedHari, $selectedSession, $bengkel)
    {
        // $selectedSession untuk menentukan bobot pada masing-masing session
        // value 1 for 08.00 - 12.00 ==> session_one pada Bengkel::class
        // value 2 for 13.00 - 16.00 ==> session_two pada Bengkel::class

        $booked = Booking::where('bengkel_id', $bengkel->id)
            ->where('hari_id', $selectedHari)
            ->where('session_id', $selectedSession)
            ->where('status', 'booked')->get();

        foreach ($booked as $key => $value) {
            $bobot += $value->bobot;
        }

        if((int)$selectedSession == 1){
            if($bobot > (int)$bengkel->session_one ){
                return FALSE;
            }
        }elseif((int)$selectedSession == 2){
            if($bobot > (int)$bengkel->session_two ){
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->logged_user = JWTAuth::toUser(Input::get('token'));
        $booking = Booking::with('layananObj', 'hari', 'user')->where('user_id', $this->logged_user->id)->get();

        $response = [
            'msg' => 'Booking information',
            'booking' => $booking,
        ];
        return response()->json($response, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->logged_user = JWTAuth::toUser(Input::get('token'));
        $validator = Validator::make($request->all(), [
            'layanan_id' => 'required',
            'bobot_layanan' => 'required',
            'hari_id' => 'required',
            'session_id' => 'required',
            'bengkel_id' => 'required',
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }

        $layanan_id = $request->input('layanan_id');
        $bobot_layanan = $request->input('bobot_layanan');
        $hari_id = $request->input('hari_id');
        $session_id = $request->input('session_id');
        $bengkel_id = $request->input('bengkel_id');

        $bengkel = Bengkel::with('layanan', 'hari')->where('id', $bengkel_id)->first();
        $harga_layanan = $bengkel->layanan->harga_layanan;
        $info_layanan = '';
        
        if(!$bengkel){
            $response = [
                'msg' => 'Bengkel not found.',
            ];
            return response()->json($response, 200);
        }

        $checkLayanan = FALSE;
        $checkHari = FALSE;
        foreach ($bengkel->layanan as $key => $layanan) {
            if($layanan_id == $layanan->id){
                $checkLayanan = TRUE;
            }
        }
        foreach ($bengkel->hari as $key => $hari) {
            if($hari_id == $hari->id){
                $checkHari = TRUE;
            }
        }
        if(!$checkLayanan){
            $response = [
                'msg' => 'Layanan not found.',
            ];
            return response()->json($response, 200);
        }
        if(!$checkHari){
            $response = [
                'msg' => 'Hari not found.',
            ];
            return response()->json($response, 200);
        }
        if(!($session_id == 1 || $session_id == 2)){
            $response = [
                'msg' => 'Booking only accept time session with id \'1\' and \'2\'.',
            ];
            return response()->json($response, 200);   
        }

        $checkBobot = $this->checkBobotLayanan($bobot_layanan,$hari_id, $session_id, $bengkel);

        if(!$checkBobot){
            $message =[
                'msg' => 'Cannot book the selected service, please choose another day or time session.',
                'bengkel' => $bengkel
            ];
            return response()->json($message, 200);
        }

        $booking = new Booking([
            'user_id'   => $this->logged_user->id,
            'kode_booking' => strtoupper(substr($bengkel->nama_bengkel,0,3) . '-' . date('Ymdhis')),
            'status'    => 'booked',
            'layanan'   => $layanan_id,
            'bobot'     => $bobot_layanan,
            'harga_layanan' => $harga_layanan,
            'info_layanan'  => $info_layanan,
            'bengkel_id'=> $bengkel->id,
            'hari_id'   => $hari_id,
            'session_id'=> $session_id,
        ]);

        if($booking->save()){
            $message =[
                'msg' => 'Success booked the serivice',
                'booked_service' => $booking
            ];
            return response()->json($message, 201);
        }

        $response = [
            'msg' => 'Error during booking.'
        ];

        return response()->json($response, 201);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $bengkel = Bengkel::with('layanan', 'hari')->where('id', $booking->bengkel_id)->first();
        if(!$booking){
            $response = [
                'msg' => 'Booked service not found.',
            ];
            return response()->json($response, 200);
        }
        if(!$bengkel){
            $response = [
                'msg' => 'Bengkel not found.',
            ];
            return response()->json($response, 200);
        }
        if($booking->status == 'booked'){
            $booking->status = 'canceled';
            if($booking->save()){
                $response = [
                    'msg' => 'User canceled the service',
                    'bengkel' => $bengkel,
                    'booking' => [
                        'href' => 'api/v1/bengkel/booking',
                        'method' => 'POST',
                        'params' => 'user_id, kode_booking, status, layanan, bobot, bengkel_id, hari_id, session_id'
                    ]
                ];

                return response()->json($response, 200);    
            }
        }elseif($booking->status == 'canceled'){
            $response = [
                'msg' => 'The service have already been canceled.',
            ];
            return response()->json($response, 200);
        }elseif($booking->status == 'done'){
            $response = [
                'msg' => 'Cannot canceled finished service.',
            ];
            return response()->json($response, 200);
        }
        
        $response = [
            'msg' => 'Error canceling the service.'
        ];

        return response()->json($response, 201);        
    }
}



