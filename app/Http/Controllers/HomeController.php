<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

use App\User;
use App\Bengkel;
use App\Booking;
use App\Layanan;
use App\Hari;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    // Untuk User::class role 1 (Administrator)
    //
    
    public function index()
    {   
        return view('admin/home');
    }

    public function bengkels()
    {
        $bengkels = Bengkel::all();
        return view('admin/bengkels', ['bengkels' => $bengkels]);
    }

    public function users()
    {
        $users = User::all();
        return view('admin/users', ['users' => $users]);
    }

    public function addUser(){
        return view('admin/create_user');
    }

    public function storeUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'no_telepon' => 'required',
            'role' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect(url()->previous())
                ->withErrors($validator)
                ->withInput();
        }

        $name = $request->input('name');
        $email = $request->input('email');
        $no_telepon = $request->input('no_telepon');
        $role = $request->input('role');
        $password = $request->input('password');

        $user = new User([
            'name' => $name,
            'email' => $email,
            'no_telepon' => $no_telepon,
            'role' => $role,
            'password' => bcrypt($password),
        ]);

        if($user->save()){
            session()->flash('success', 'Success created new user.');    
            return redirect()->back();
        }
    }

    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    // Untuk User::class role 2 (Bengkel User)
    //

    public function profile()
    {
        $logged_user = User::find(Auth::id());
        $bengkel = Bengkel::with('layanan', 'hari')->where('id', $logged_user->bengkel_id)->first();
        return view('bengkel/profile', ['bengkel' => $bengkel]);   
    }

    public function create()
    {
        $logged_user = User::find(Auth::id());
        if($logged_user->bengkel_id != null){
            return redirect('home/bengkel');
        }
        return view('bengkel/create');   
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_bengkel' => 'required',
            'lokasi_bengkel' => 'required',
            'foto_bengkel' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'session_one' => 'required',
            'session_two' => 'required',
            'user_id' => 'required|unique:bengkels,user_id'
        ]);
        
        if ($validator->fails()) {
            return redirect(url()->previous())
                ->withErrors($validator)
                ->withInput();
        }

        if($request->file('foto_bengkel') != NULL){
            $extension = $request->file('foto_bengkel')->getClientOriginalExtension();
            $name = date('Y-m-d-h-i-s');
            $storage_folder = 'bengkel/';
            $photo_name = $name . '.' . $extension;
            $foto_bengkel = $storage_folder . $photo_name;

            $request->file('foto_bengkel')->storeAs(
                'public/bengkel', $photo_name
            );
        }else{
            $foto_bengkel = '';
        }

        $nama_bengkel = $request->input('nama_bengkel');
        $lokasi_bengkel = $request->input('lokasi_bengkel');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $session_one = $request->input('session_one');
        $session_two = $request->input('session_two');
        $foto_bengkel = $foto_bengkel;
        $user_id = $request->input('user_id');

        $form_params = [
            'nama_bengkel' => $nama_bengkel,
            'lokasi_bengkel' => $lokasi_bengkel,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'session_one' => $session_one,
            'session_two' => $session_two,
            'foto_bengkel' => $foto_bengkel,
            'user_id' => $user_id
        ];

        $bengkel = new Bengkel ($form_params);

        if($bengkel->save() && $bengkel->updateUser($user_id, $bengkel->id)){
            session()->flash('success', 'Success created new bengkel.');    
            return redirect()->route('home/bengkel/profile');
        }
    }

    public function profileUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_bengkel' => 'required',
            'lokasi_bengkel' => 'required',
            'foto_bengkel' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'session_one' => 'required',
            'session_two' => 'required',
            'bengkel_id' => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect(url()->previous())
                ->withErrors($validator)
                ->withInput();
        }

        if($request->file('foto_bengkel') != NULL){
            $extension = $request->file('foto_bengkel')->getClientOriginalExtension();
            $name = date('Y-m-d-h-i-s');
            $storage_folder = 'bengkel/';
            $photo_name = $name . '.' . $extension;
            $foto_bengkel = $storage_folder . $photo_name;

            $request->file('foto_bengkel')->storeAs(
                'public/bengkel', $photo_name
            );
        }else{
            $foto_bengkel = '';
        }

        $nama_bengkel = $request->input('nama_bengkel');
        $lokasi_bengkel = $request->input('lokasi_bengkel');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $session_one = $request->input('session_one');
        $session_two = $request->input('session_two');
        $foto_bengkel = $foto_bengkel;
        $bengkel_id = $request->input('bengkel_id');

        $bengkel = Bengkel::find($bengkel_id);

        $bengkel->nama_bengkel = $nama_bengkel;
        $bengkel->lokasi_bengkel = $lokasi_bengkel;
        $bengkel->latitude = $latitude;
        $bengkel->longitude = $longitude;
        $bengkel->foto_bengkel = $foto_bengkel;
        $bengkel->session_one = $session_one;
        $bengkel->session_two = $session_two;


        if($bengkel->update()){
            session()->flash('success', 'Success updated bengkel.');    
            return redirect()->back();
        }
    }

    public function addLayanan(Request $request){
        $validator = Validator::make($request->all(), [
            'nama_layanan' => 'required',
            'bobot_layanan' => 'required',
            'harga_layanan' => 'required',
            'bengkel_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect(url()->previous())
                ->withErrors($validator)
                ->withInput();
        }

        $nama_layanan = $request->input('nama_layanan');
        $bobot_layanan = $request->input('bobot_layanan');
        $harga_layanan = $request->input('harga_layanan');
        $bengkel_id = $request->input('bengkel_id');
            
        $layanan = new Layanan;
        $layanan->nama_layanan = $nama_layanan;
        $layanan->bobot_layanan = $bobot_layanan;
        $layanan->harga_layanan = $harga_layanan;
        $layanan->bengkel_id = $bengkel_id;

        if($layanan->save()){
            session()->flash('success', 'Success created new service.');    
            return redirect()->back();
        }
    }

    public function addHari(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hari_layanan' => 'required',
            'bengkel_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect(url()->previous())
                ->withErrors($validator)
                ->withInput();
        }

        $hari_layanan = $request->input('hari_layanan');
        $bengkel_id = $request->input('bengkel_id');
  
        $hari = new Hari;
        $hari->hari = $hari_layanan;
        $hari->bengkel_id = $bengkel_id;

        if($hari->save()){
            session()->flash('success', 'Success add new service day.');    
            return redirect()->back();
        }
    }

    public function deleteLayanan($id){
        $layanan = layanan::find($id);

        if($layanan->delete()){
            return redirect()->back();
        }
    }

    public function deleteHari($id){
        $hari = Hari::find($id);

        if($hari->delete()){
            return redirect()->back();
        }
    }
    public function booking(){
        $logged_user = User::find(Auth::id());
        $bookings = Booking::where('bengkel_id', $logged_user->bengkel_id)->get();
        return view('bengkel/booking', ['bookings' => $bookings]);
    }

    public function finishBooking($id)
    {   
        $booking = Booking::find($id);

        if($booking->status == 'canceled'){
            session()->flash('error', 'Cannot finish "canceled booking"');    
            return redirect()->back();
        }elseif($booking->status == 'done'){
            session()->flash('error', 'Booking status is already "done".');    
            return redirect()->back();
        }   
        
        $booking->status = 'done';

        if($booking->update()){
            session()->flash('success', 'Booking status updated');    
            return redirect()->back();
        }else{
            session()->flash('error', 'Cannot update booking status');    
            return redirect()->back();
        }
    }

    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    // Untuk User::class role 3 (Customer)
    //
}
