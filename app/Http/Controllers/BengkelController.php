<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
use JWTAuth;

use App\Bengkel;
use App\User;

class BengkelController extends Controller
{
    protected $logged_user;

    public function __construct(){
        // $this->logged_user = JWTAuth::toUser(Input::get('token'));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bengkels = Bengkel::all();
        foreach($bengkels as $bengkel){
            $bengkel->view_bengkel =[
                'href' => 'api/v1/bengkel/' . $bengkel->id,
                'method' => 'GET'
            ];
        }

        $response = [
            'msg' =>'List of all bengkels',
            'bengkels' => $bengkels,
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_bengkel' => 'required',
            'lokasi_bengkel' => 'required',
            'foto_bengkel' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'session_one' => 'required',
            'session_two' => 'required',
            'user_id' => 'required|unique:bengkels,user_id'
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }
        
        if($request->file('foto_bengkel') != NULL){
            $extension = $request->file('foto_bengkel')->getClientOriginalExtension();
            $name = date('Y-m-d-h-i-s');
            $storage_folder = 'bengkel/';
            $photo_name = $name . '.' . $extension;
            $foto_bengkel = $storage_folder . $photo_name;

            $request->file('foto_bengkel')->storeAs(
                'public/bengkel', $photo_name
            );
        }else{
            $foto_bengkel = '';
        }

        $nama_bengkel = $request->input('nama_bengkel');
        $lokasi_bengkel = $request->input('lokasi_bengkel');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $session_one = $request->input('session_one');
        $session_two = $request->input('session_one');
        $user_id = $request->input('user_id');

        $bengkel = new Bengkel([
            'nama_bengkel' => $nama_bengkel,
            'lokasi_bengkel' => $lokasi_bengkel,
            'foto_bengkel' => $foto_bengkel,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'session_one' => $session_one,
            'session_two' => $session_two,
            'user_id' => $user_id,
        ]);

        if($bengkel->save() && $bengkel->updateUser($user_id, $bengkel->id)){
            $bengkel->view_bengkel = [
                'href' => 'api/v1/bengkel/1',
                'method' => 'GET'
            ];
            $message =[
                'msg' => 'Bengkel created',
                'bengkel' => $bengkel,
            ];
            return response()->json($message, 201);
        }

        $response = [
            'msg' => 'Error during creation'
        ];

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bengkel = Bengkel::with('layanan', 'hari')->where('id', $id)->first();

        if(!$bengkel){
            $response = [
                'msg' => 'Bengkel not found.',
            ];
            return response()->json($response, 200);
        }

        $bengkel->view_bengkel =[
            'href' => 'api/v1/bengkel',
            'method' => 'GET'
        ];

        $response = [
            'msg' => 'Bengkel information',
            'bengkel' => $bengkel,
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_bengkel' => 'required',
            'lokasi_bengkel' => 'required',
            'foto_bengkel' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'session_one' => 'required',
            'session_two' => 'required',
            'user_id' => 'required'
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }

        if($request->file('foto_bengkel') != NULL){
            $extension = $request->file('foto_bengkel')->getClientOriginalExtension();
            $name = date('Y-m-d-h-i-s');
            $storage_folder = 'bengkel/';
            $photo_name = $name . '.' . $extension;
            $foto_bengkel = $storage_folder . $photo_name;

            $request->file('foto_bengkel')->storeAs(
                'public/bengkel', $photo_name
            );
        }else{
            $foto_bengkel = '';
        }

        $nama_bengkel = $request->input('nama_bengkel');
        $lokasi_bengkel = $request->input('lokasi_bengkel');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $session_one = $request->input('session_one');
        $session_two = $request->input('session_two');
        $foto_bengkel = $foto_bengkel;
        $user_id = $request->input('user_id');

        $bengkel = Bengkel::find($id);
        if(!$bengkel){
            return response()->json([
                'msg' => 'Bengkel not found.'
            ], 401);
        }

        if($bengkel->user_id != $user_id){
            return response()->json([
                'msg' => 'user is not registered for this bengkel, cannot update bengkel.'
            ], 401);
        };
        
        $bengkel->nama_bengkel = $nama_bengkel;
        $bengkel->lokasi_bengkel = $lokasi_bengkel;
        $bengkel->latitude = $latitude;
        $bengkel->longitude = $longitude;
        $bengkel->foto_bengkel = $foto_bengkel;
        $bengkel->session_one = $session_one;
        $bengkel->session_two = $session_two;

        if(!$bengkel->update()){
            return response()->json([
                'msg' => 'Error during update'
            ], 404);
        }
        
        $bengkel->view_bengkel = [
            'href' => 'api/v1/bengkel/' . $bengkel->id,
            'method' => 'GET'
        ];

        $response = [
            'msg' => 'Bengkel Updated',
            'bengkel' => $bengkel
        ];

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bengkel = Bengkel::find($id);
        if(!$bengkel){
            $response = [
                'msg' => 'Bengkel not found.',
            ];
            return response()->json($response, 200);
        }  
        
        if($bengkel->delete()){
            $response = [
                'msg' => 'Bengkel deleted',
                'create' => [
                    'href' => 'api/v1/bengkel',
                    'method' => 'POST',
                    'params' => 'nama_bengkel, lokasi_bengkel, foto_bengkel, latitude, longitude, session_one, session_two, user_id'
                ]
            ];
            return response()->json($response, 200);
        }

        $response = [
            'msg' => 'Error during deletion'
        ];

        return response()->json($response, 201);
    }
}









