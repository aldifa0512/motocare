<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

use JWTAuth;
use JWTAuthException;

class AuthController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
            'email' =>'required|email|unique:users,email|max:30',
            'password' => 'required|min:5|max:30',
            'no_telepon' => 'required|max:15',
            // 'role' => 'required|numeric',
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $no_telepon = $request->input('no_telepon');
        $role = 3; //$request->input('role');

        $user = new User ([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'no_telepon' => $no_telepon,
            'role' => $role
        ]);

        $credentials =[
            'email' => $email,
            'password' => $password
        ];

        if($user->save()){
            
            $token = null;
            try{
                if(!$token = JWTAuth::attempt($credentials)){
                    return response()->json([
                        'msg' => 'Email or Password are incorrect',
                    ], 404);
                }
            }catch(JWTAuthException $e){
                return response()->json([
                    'msg' => 'failed_to_create_token',
                ], 404);
            }

            $user->signin =[
                'href' => 'api/v1/user/signin',
                'method' =>'POST',
                'params' => 'email, password'
            ];
            
            $response =[
                'msg' => 'User created',
                'user' => $user,
                'token' => $token
            ];
            return response()->json($response, 201);
        }

        $response = [
            'msg' => 'An error occurred'
        ];

        return response()->json($response, 404);
    }

	public function signin(Request $request)
	{
        $validator = Validator::make($request->all(), [
            'email' =>'required|email',
            'password' => 'required|min:5'
        ]);
        
        if ($validator->fails()) {
          return response()->json(['errors'=>$validator->errors()]);
        }
        
        $email = $request->input('email');
        $password = $request->input('password');


        if($user = User::where('email', $email)->first()){
            
            $credentials =[
                'email' => $email,
                'password' => $password
            ];

            $token = null;
            try{
                if(!$token = JWTAuth::attempt($credentials)){
                    return response()->json([
                        'msg' => 'Email or Password are incorrect',
                    ], 404);
                }
                if($user->role != 3){
                    return response()->json([
                        'msg' => 'Administrator and Bengkel User cannot signin to this service.',
                    ], 404);
                }
            }catch(JWTAuthException $e){
                return response()->json([
                    'msg' => 'failed_to_create_token',
                ], 404);
            }
            
            $response =[
                'msg' => 'User signed',
                'user' => $user,
                'token' => $token
            ];
            return response()->json($response, 201);
        }

        $response = [
            'msg' => 'An error occurred, email not found.'
        ];

        return response()->json($response, 404);
	}
}
