<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Bengkel extends Model
{
    protected $fillable = [
    	'nama_bengkel', 
    	'lokasi_bengkel', 
    	'layanan_bengkel', 
    	'foto_bengkel', 
    	'latitude', 
    	'longitude', 
    	'session_one', 
    	'session_two', 
    	'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'bengkel_id');
    }

    public function layanan()
    {
    	return $this->hasMany(Layanan::class, 'bengkel_id', 'id');
    }

    public function hari()
    {
        return $this->hasMany(Hari::class, 'bengkel_id', 'id');
    }

    public function updateUser($id, $bengkel_id)
    {
    	$updateUser = DB::table('users')
            ->where('id', $id)
            ->update([
            	'bengkel_id' => $bengkel_id
            ]);

        if($updateUser){
        	return TRUE;
        }
        return FALSE;
    }
}
