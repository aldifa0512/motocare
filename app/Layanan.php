<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    protected $table = 'layanan_bengkel';

    public function bengkel()
    {
        return $this->belongsTo(Bengekl::class);
    }
}
