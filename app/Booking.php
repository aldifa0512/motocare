<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';

    protected $fillable = [
    		'user_id',   
            'kode_booking', 
            'status',   
            'layanan',   
            'bobot',     
            'harga',     
            'bengkel_id',
            'hari_id',   
            'session_id'
        ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function layananObj()
    {
        return $this->belongsTo(Layanan::class,'layanan', 'id');
    }

    public function hari()
    {
        return $this->belongsTo(Hari::class);
    }

    public function session($id)
    {
        if($id == 1){
            return '08:00 - 12:00';
        }elseif($id == 2){
            return '13:00 - 16:00';
        }   
    }
}
