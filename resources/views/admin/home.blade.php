@extends('layouts.admin')

@section('title', 'Home')

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/fullcalendar.css" />
    <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
    <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
    <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/jquery.gritter.css" />
@stop

@section('inline-style')

@stop

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

     <div id="content">
        <div id="content-header">
          <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
          </div>
          <h1>Home</h1>
        </div>

    </div>
@endsection

@section('page-scripts')
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/excanvas.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.flot.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.flot.resize.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.peity.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/fullcalendar.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.dashboard.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.gritter.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.interface.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.chat.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.validate.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.form_validation.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.wizard.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.popover.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.dataTables.min.js"></script> 
    <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.tables.js"></script> 
@stop

@section('inline-script')
    <script type="text/javascript">
      // This function is called from the pop-up menus to transfer to
      // a different page. Ignore if the value returned is a null string:
      function goPage (newURL) {

          // if url is empty, skip the menu dividers and reset the menu selection to default
          if (newURL != "") {
          
              // if url is "-", it is this page -- reset the menu:
              if (newURL == "-" ) {
                  resetMenu();            
              } 
              // else, send page to designated URL            
              else {  
                document.location.href = newURL;
              }
          }
      }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
       document.gomenu.selector.selectedIndex = 2;
    }
    </script>
@stop