@extends('layouts.admin')

@section('title', 'Bengkel')

@section('page-styles')
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/uniform.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/select2.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
  <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
@stop

@section('inline-style')

@stop

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"> <a href="{{ route('home') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('home/bengkels') }}" class="current">Bengkel</a> </div>
    </div>
    <div class="container-fluid">
      <hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>List of Bengkel</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>Nama Bengkel</th>
                    <th>Lokasi</th>
                    <th width="70">Foto</th>
                    <th>Owner</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Session One</th>
                    <th>Session Two</th>
                    <th>Created Date</th>
                    <!-- <th width="100px;">Action</th> -->
                  </tr>
                </thead>
                <tbody>
                  @foreach ($bengkels as $bengkel)
                  <tr class="gradeX">
                    <td>{{ $bengkel->nama_bengkel }}</td>
                    <td>{{ $bengkel->lokasi_bengkel }}</td>
                    <td><img src="{{ asset('storage/' . $bengkel->foto_bengkel) }}"></td>
                    <td>{{ $bengkel->user->name }}</td>
                    <td>{{ $bengkel->latitude }}</td>
                    <td>{{ $bengkel->longitude }}</td>
                    <td>{{ $bengkel->session_one }}</td>
                    <td>{{ $bengkel->session_two }}</td>
                    <td>{{ $bengkel->created_at }}</td>
                    <!-- <td>
                      <a href="{{ url()->current() . '/edit/' . $bengkel->id }}"><button class="btn btn-warning btn-mini">Edit</button></a>
                      <a href="{{ url()->current() . '/delete/' . $bengkel->id }}" onclick="return confirm('Are you sure you want to delete this bengkel?');"><button class="btn btn-danger btn-mini">Delete</button></a>
                    </td> -->
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('page-scripts')
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.dataTables.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.tables.js"></script>
@stop

@section('inline-script')

@stop