@extends('layouts.admin')

@section('title', 'Users')

@section('page-styles')
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/uniform.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/select2.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
  <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
@stop

@section('inline-style')

@stop

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"> <a href="{{ route('home') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('home/users') }}" class="current">Users</a> </div>
    </div>
    <div class="container-fluid">
      <h4><a href="{{ url()->current() . '/add' }}"><button class="btn btn-primary">Add User</button></a></h4>
      <hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>List of Users</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>Nama User</th>
                    <th>Email</th>
                    <th>No Telp</th>
                    <th>Role</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                  <tr class="gradeX">
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->no_telepon }}</td>
                    <td>{{ $user->role }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->updated_at }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('page-scripts')
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.dataTables.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.tables.js"></script>
@stop

@section('inline-script')

@stop