@extends('layouts.admin')

@section('title', 'Create New User')

@section('page-styles')
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/colorpicker.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/datepicker.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/uniform.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/select2.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-wysihtml5.css" />
  <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
@stop

@section('inline-style')

@stop

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ route('home') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('home/users') }}" class="tip-bottom">Bengkel</a> <a href="{{ route('home/users/add') }}" class="current">Create New user</a> </div>
  <h1>Create User Form</h1>
  @if ($errors->any())
  <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{!! $error !!}</li>
              @endforeach
          </ul>
      </div>
  @endif
  @if (session('success'))
      <div class="alert alert-success">
          {!! session('success') !!}
      </div>
  @endif
  @if (session('error'))
      <div class="alert alert-danger">
          {!! session('error') !!}
      </div>
  @endif
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Data User</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url()->current() }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }} 
            <input type="hidden" name="role" class="span11" placeholder="" value="2" />
            <div class="control-group">
              <label class="control-label">Name </label>
              <div class="controls">
                <input type="text" name="name" value="{{ old('name') }}" class="span11" placeholder="Name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email</label>
              <div class="controls">
                <input type="email" name="email" value="{{ old('email') }}" class="span11" placeholder="Email" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">No Telp</label>
              <div class="controls">
                <input type="number" name="no_telepon" value="{{ old('no_telepon') }}" class="span11" placeholder="No Telepon" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Password</label>
              <div class="controls">
                <input type="password" name="password" value="{{ old('password') }}" class="span11" placeholder="Password" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Confirm Password</label>
              <div class="controls">
                <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="span11" placeholder="Password Confirmation" />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div></div>

@endsection

@section('page-scripts')
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-colorpicker.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-datepicker.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.toggle.buttons.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/masked.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.form_common.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/wysihtml5-0.3.0.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.peity.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-wysihtml5.js"></script> 
@stop

@section('inline-script')
<script>
  $('.textarea_editor').wysihtml5();
</script>
@stop