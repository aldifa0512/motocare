<?php

//logged user
$logged_user = App\User::find(Auth::id());

// get active bar function hahahah 
function getActiveBar(){
  
  $url = Route::currentRouteName();
  $currentUrl = explode('/', $url);  
  
  if($url == 'home'){
    return $currentUrl[0];
  }

  $return = '';
  foreach ($currentUrl as $key => $value) {
    if($key == 0 ) continue;
    $return .= '/' . $value;
  }
  $return = substr($return, 1);
  return $return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="icon" href="{{ asset('admin/matrix-admin-package/html') }}/img/icons/32/web.png" type="image/x-icon">

@yield('page-styles')

@yield('inline-style')

</head>
<body>

<!--Header-part-->
<div id="header">
  <h1><a href="{{ url('/') }}"><img src="{{ url('/components/images/logo_signup.png') }}" style="max-width: 100px" class=""></a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="{{ asset('admin/matrix-admin-package/html') }}/#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">{{ Auth::user()->name }}</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="divider"></li>
        <li class="">
          <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
              <i class="icon icon-share-alt"></i> Logout
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    @if ($logged_user->role == 1)
      <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
      <li class="{{ getActiveBar() == 'bengkels' ? 'active' : '' }}"> <a href="{{ route('home/bengkels') }}"><i class="icon icon-inbox"></i> <span>Daftar Bengkel</span></a> </li>
      <li class="{{ getActiveBar() == 'users' ? 'active' : '' }}"> <a href="{{ route('home/users') }}"><i class="icon icon-file"></i> <span>Daftar User</span></a> </li>
    @elseif ($logged_user->role == 2)
      <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
      @if ($logged_user->bengkel_id == null)
        <li class="{{ getActiveBar() == 'bengkel/create' ? 'active' : '' }}"> <a href="{{ route('home/bengkel/create') }}"><i class="icon icon-inbox"></i> <span>Create Bengkel</span></a> </li>
      @else
        <li class="{{ getActiveBar() == 'bengkel/profile' ? 'active' : '' }}"> <a href="{{ route('home/bengkel/profile') }}"><i class="icon icon-file"></i> <span>Profile Bengkel</span></a> </li>
        <li class="{{ getActiveBar() == 'bengkel/booking' ? 'active' : '' }}"> <a href="{{ route('home/bengkel/booking') }}"><i class="icon icon-file"></i> <span>Booking</span></a> </li>
      @endif
    @elseif ($logged_user->role == 3)
      <li class="{{ Route::currentRouteName() == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    @endif  
    
  </ul>
</div>
<!--sidebar-menu-->

@yield('content')

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="{{ asset('admin/matrix-admin-package/html') }}/http://themedesigner.in">Themedesigner.in</a> </div>
</div>

<!--end-Footer-part-->

@yield('page-scripts')

@yield('inline-script')

</body>
</html>
