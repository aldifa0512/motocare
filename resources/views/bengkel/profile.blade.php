@extends('layouts.admin')

@section('title', 'Profile Bengkel')

@section('page-styles')
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/colorpicker.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/datepicker.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/uniform.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/select2.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-wysihtml5.css" />
  <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
@stop

@section('inline-style')

@stop

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ route('home') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('home/bengkel/profile') }}" class="tip-bottom">Bengkel</a> <a href="{{ route('home/bengkel/create') }}" class="current">Profile Bengkel</a> </div>
  <h1>Profile Bengkel Form</h1>
  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  @if (session('success'))
      <div class="alert alert-success">
          {{ session('success') }}
      </div>
  @endif
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Data Bengkel</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url()->current() }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }} 
            <input type="hidden" name="bengkel_id" class="span11" placeholder="" value="{{ $bengkel->id }}" />
            <div class="control-group">
              <label class="control-label">Nama Bengkel</label>
              <div class="controls">
                <input type="text" name="nama_bengkel" value="{{ $bengkel->nama_bengkel }}" class="span11" placeholder="Nama Bengkel" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">lokasi Bengkel</label>
              <div class="controls">
                <input type="text" name="lokasi_bengkel" value="{{ $bengkel->lokasi_bengkel }}" class="span11" placeholder="Lokasi Bengkel" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Latitude</label>
              <div class="controls">
                <input type="number" name="latitude" value="{{ $bengkel->latitude }}" class="span11" placeholder="Latitude" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Longitude</label>
              <div class="controls">
                <input type="number" name="longitude" value="{{ $bengkel->longitude }}" class="span11" placeholder="Longitude" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bobot Time Session 1</label>
              <div class="controls">
                <input type="number" name="session_one" value="{{ $bengkel->session_one }}" class="span11" placeholder="Bobot Time Session 1" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bobot Time Session 2</label>
              <div class="controls">
                <input type="number" name="session_two" value="{{ $bengkel->session_two }}" class="span11" placeholder="Bobot Time Session 2" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Foto Bengkel</label>
              <div class="controls">
                @if ($bengkel->foto_bengkel != NULL)
                    <img src="{{ asset('storage/' . $bengkel->foto_bengkel) }}" style="max-width: 100px;"><br>
                    <input type="file" name="foto_bengkel" />
                @else
                    <input type="file" name="foto_bengkel" />
                @endif
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save Post</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Data Layanan</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url()->current() . '/layanan' }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }} 
            <input type="hidden" name="bengkel_id" class="span11" placeholder="" value="{{ $bengkel->id }}" />
            <div class="control-group">
              <label class="control-label">Nama Layanan</label>
              <div class="controls">
                <input type="text" name="nama_layanan" value="{{ old('nama_layanan') }}" class="span11" placeholder="Nama Layanan" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bobot Layanan</label>
              <div class="controls">
                <input type="number" name="bobot_layanan" value="{{ old('bobot_layanan') }}" class="span11" placeholder="Bobot Layanan" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Harga Layanan</label>
              <div class="controls">
                <input type="number" name="harga_layanan" value="{{ old('harga_layanan') }}" class="span11" placeholder="Harga Layanan" />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save Post</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Tambah Hari Layanan</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url()->current() . '/hari' }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }} 
            <input type="hidden" name="bengkel_id" class="span11" placeholder="" value="{{ $bengkel->id }}" />
            <div class="control-group">
              <label class="control-label">Hari Layanan</label>
              <div class="controls">
                <input type="text" name="hari_layanan" value="{{ old('hari_layanan') }}" class="span11" placeholder="Hari Layanan" />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save Post</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <hr>
  <h3>Data Layanan dan Hari</h3>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
          <h5>Layanan</h5>
        </div>
        <div class="widget-content nopadding">
          <table class="table table-bordered data-table">
            <thead>
              <tr>
                <th>Nama Layanan</th>
                <th>Bobot Layanan</th>
                <th>Harga Layanan</th>
                <th width="100px;">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($bengkel->layanan as $value)
              <tr class="gradeX">
                <td>{{ $value->nama_layanan }}</td>
                <td>{{ $value->bobot_layanan }}</td>
                <td>{{ number_format($value->harga_layanan) }}</td>
                <td>
                  <a href="{{ url()->current() . '/layanan/delete/' . $value->id }}" onclick="return confirm('Are you sure you want to delete this service?');"><button class="btn btn-danger btn-mini">Delete</button></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
          <h5>Hari Layanan</h5>
        </div>
        <div class="widget-content nopadding">
          <table class="table table-bordered data-table">
            <thead>
              <tr>
                <th>Hari Layanan</th>
                <th width="100px;">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($bengkel->hari as $value)
              <tr class="gradeX">
                <td>{{ $value->hari }}</td>
                <td>
                  <a href="{{ url()->current() . '/hari/delete/' . $value->id }}" onclick="return confirm('Are you sure you want to delete this service?');"><button class="btn btn-danger btn-mini">Delete</button></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div></div>

@endsection

@section('page-scripts')
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-colorpicker.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-datepicker.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.toggle.buttons.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/masked.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.form_common.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/wysihtml5-0.3.0.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.peity.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-wysihtml5.js"></script> 
@stop

@section('inline-script')
<script>
  $('.textarea_editor').wysihtml5();
</script>
@stop