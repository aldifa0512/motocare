@extends('layouts.admin')

@section('title', 'Create Bengkel')

@section('page-styles')
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/colorpicker.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/datepicker.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/uniform.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/select2.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-wysihtml5.css" />
  <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
@stop

@section('inline-style')

@stop

@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="{{ route('home') }}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('home/bengkel/profile') }}" class="tip-bottom">Bengkel</a> <a href="{{ route('home/bengkel/create') }}" class="current">Create Bengkel</a> </div>
  <h1>Create Bengkel Form</h1>
  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  @if (session('success'))
      <div class="alert alert-success">
          {{ session('success') }}
      </div>
  @endif
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Data Bengkel</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{ url()->current() }}" method="post" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }} 
            <input type="hidden" name="user_id" class="span11" placeholder="" value="{{ Auth::id() }}" />
            <div class="control-group">
              <label class="control-label">Nama Bengkel</label>
              <div class="controls">
                <input type="text" name="nama_bengkel" value="{{ old('nama_bengkel') }}" class="span11" placeholder="Nama Bengkel" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">lokasi Bengkel</label>
              <div class="controls">
                <input type="text" name="lokasi_bengkel" value="{{ old('lokasi_bengkel') }}" class="span11" placeholder="Lokasi Bengkel" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Latitude</label>
              <div class="controls">
                <input type="number" name="latitude" value="{{ old('latitude') }}" class="span11" placeholder="Latitude" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Longitude</label>
              <div class="controls">
                <input type="number" name="longitude" value="{{ old('longitude') }}" class="span11" placeholder="Longitude" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bobot Time Session 1</label>
              <div class="controls">
                <input type="number" name="session_one" value="{{ old('session_one') }}" class="span11" placeholder="Bobot Time Session 1" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bobot Time Session 2</label>
              <div class="controls">
                <input type="number" name="session_two" value="{{ old('session_two') }}" class="span11" placeholder="Bobot Time Session 2" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Foto Bengkel</label>
              <div class="controls">
                <input type="file" name="foto_bengkel" value="{{ old('foto_bengkel') }}" />
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save Post</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div></div>

@endsection

@section('page-scripts')
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-colorpicker.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-datepicker.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.toggle.buttons.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/masked.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.form_common.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/wysihtml5-0.3.0.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.peity.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap-wysihtml5.js"></script> 
@stop

@section('inline-script')
<script>
  $('.textarea_editor').wysihtml5();
</script>
@stop