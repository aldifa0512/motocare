@extends('layouts.admin')

@section('title', 'Boooking')

@section('page-styles')
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/uniform.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/select2.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-style.css" />
  <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-media.css" />
  <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
@stop

@section('inline-style')

@stop

@section('content')


  
  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{ route('home/bengkel/profile') }}" class="current">Profile Bengkel</a> </div>
    </div>
    <div class="container-fluid">
      <hr>
      @if ($errors->any())
      <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{!! $error !!}</li>
                  @endforeach
              </ul>
          </div>
      @endif
      @if (session('success'))
          <div class="alert alert-success">
              {!! session('success') !!}
          </div>
      @endif
      @if (session('error'))
          <div class="alert alert-danger">
              {!! session('error') !!}
          </div>
      @endif
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>List of Booked Services</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>User</th>
                    <th>Kode Booking</th>
                    <th>Status</th>
                    <th>Layanan</th>
                    <th>Bobot Layanan</th>
                    <th>Harga Layanan</th>
                    <th>Info Layanan</th>
                    <th>Hari</th>
                    <th>Session</th>
                    <th>Created At</th>
                    <th width="100px;">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($bookings as $booking)
                  <tr class="gradeX">
                    <td>{{ $booking->user->name }}</td>
                    <td>{{ $booking->kode_booking }}</td>
                    <td>{{ $booking->status }}</td>
                    <td>{{ $booking->layananObj->nama_layanan }}</td>
                    <td>{{ $booking->bobot }}</td>
                    <td>{{ $booking->harga_layanan }}</td>
                    <td>{{ $booking->info_layanan }}</td>
                    <td>{{ $booking->hari->hari }}</td>
                    <td>{{ $booking->session($booking->session_id) }}</td>
                    <td>{{ $booking->created_at }}</td>
                    <td>
                      <a href="{{ url()->current() . '/done/' . $booking->id }}" onclick="return confirm('Are you sure you want to finish this booking?');"><button class="btn btn-primary btn-mini">Done</button></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('page-scripts')
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.ui.custom.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/bootstrap.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.uniform.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/select2.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.dataTables.min.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.js"></script> 
  <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.tables.js"></script>
@stop

@section('inline-script')

@stop