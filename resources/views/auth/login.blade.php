<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Login Motocare</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap.min.css" />
        <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="{{ asset('admin/matrix-admin-package/html') }}/css/matrix-login.css" />
        <link href="{{ asset('admin/matrix-admin-package/html') }}/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <link rel="icon" href="{{ asset('admin/matrix-admin-package/html') }}/img/icons/32/web.png" type="image/x-icon">
        <style type="text/css">
           @media only screen and (max-width: 480px), only screen and (max-device-width: 480px) {
                .logoLogin{
                    width: 90%;
                }
            }
        </style>

    </head>
    <body style="background-color: #c82a2a;">
        <div id="loginbox" style="background-color: #d5dae2;">            
            <form class="form-vertical"  id="loginform" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="control-group normal_text"> <h3><img class="logoLogin" style="max-width: 90%;" src="{{ asset('admin/matrix-admin-package/html') }}/img/motocare.png" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email" />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input id="password" type="password" class="form-control" name="password" required placeholder="Password" />
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <center><button style="width: 90%;" type="submit" class="btn btn-success">Login</button></center>
                </div>
                </div>
            </form>
            <center>&#9400; Copyright Motocare</center>
        </div>
        
        <script src="{{ asset('admin/matrix-admin-package/html') }}/js/jquery.min.js"></script>  
        <script src="{{ asset('admin/matrix-admin-package/html') }}/js/matrix.login.js"></script> 
    </body>

</html>

