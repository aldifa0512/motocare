<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect('login');    
});

Auth::routes();

Route::group(['middleware' => ['auth'], 'prefix' => 'home'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    
    // Untuk Administrator $role = 1
    Route::get('bengkels', 'HomeController@bengkels')->name('home/bengkels');
    Route::get('users', 'HomeController@users')->name('home/users');
    Route::get('users/add', 'HomeController@addUser')->name('home/users/add');

    Route::post('users/add', 'HomeController@storeUser')->name('home/users/store');

    // Untuk User Bengkel $role = 2
    Route::group(['prefix' => 'bengkel'], function () {
        Route::get('/', 'HomeController@profile')->name('home/bengkel/profile');
        Route::get('create', 'HomeController@create')->name('home/bengkel/create');

        Route::get('booking', 'HomeController@booking')->name('home/bengkel/booking');
        Route::get('booking/done/{id}', 'HomeController@finishBooking');

        Route::get('layanan/delete/{id}', 'HomeController@deleteLayanan');
        Route::get('hari/delete/{id}', 'HomeController@deleteHari');

        Route::post('/', 'HomeController@profileUpdate');
        Route::post('layanan', 'HomeController@addLayanan');
        Route::post('hari', 'HomeController@addHari');
        Route::post('create', 'HomeController@store');
        
    });
});
